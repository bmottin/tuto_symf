# Installation du projet

1. Vich.uploader-bundle
simplifie l'upload de fichier
`composer require vich/uploader-bundle`

répondre Yes à la question posée. 

2. Imagine-bundle
permet de mettre les images en cache suivant le contexte pour optimiser le chargement
`composer require liip/imagine-bundle`

répondre Yes à la question posée. 


3. TODO ckeditor
`composer require friendsofsymfony/ckeditor-bundle`

/!\ bien penser a faire l'installation

`php bin/console ckeditor:install`

`php bin/console assets:install public`

4. migration et lancement du serveur

vérifier les information dans le .env
Une fois tout les étapes de l'install. 
faites les commandes standards de migration du projet

s'il n'y a pas la base de données à utiliser vous devez la creer 
grâce à la commande :
`php bin/console doctrine:database:create`

puis supprimmer tout les fichiers present dans src/migrations et lancer la commande:
`php bin/console make:migration`

qui va generer les fichiers php avec les requêtes SQL
`php bin/console doctrine:migrations:migrate`

lancer le serveur en dev
`symfony server:start`

le site doit alors être accessible sur l'adresse : localhost:8000


