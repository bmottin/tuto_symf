<?php

namespace App\Controller;

use App\Repository\ArticleImageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ArticleImageController extends AbstractController
{
    /**
     * @Route("/articleImage", name="ck_article_image_repo")
     */
    public function articleImage(ArticleImageRepository $articleImageRepository)
    {
        return $this->render('article_image/ck_article_image_repo.html.twig', [
            'controller_name' => 'ArticleImageController',
            'images' => $articleImageRepository->findAll()
        ]);
    }
}
