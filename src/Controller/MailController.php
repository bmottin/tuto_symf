<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class MailController extends AbstractController
{
    /**
     * @Route("/mailTest", name="mailtest")
     */
    public function mailTest(MailerInterface $mailer)
    {
        $email = (new Email())
            ->from("MonProejt@test.com")
            ->to("client@client.com")
            ->subject("Validation et test")
            ->text("Ceci est un mail de test")
            ->html("<h2>Coucou</h2><p>Un mail test mais en <strong>html</html></p>");

        $mailer->send($email);

        return new Response("", Response::HTTP_OK);
    }

    /**
     * @Route("/mailRegister", name="mailregister")
     */
    public function mailRegister(MailerInterface $mailer, $to, $username, $validationCode)
    {
        $url = $this->generateUrl("validationEmail", [
            'email'=>$to, 
            'validationCode'=>$validationCode
        ], UrlGeneratorInterface::ABSOLUTE_URL);

        $email = (new Email())
            ->from("contact@monblog.com")
            ->to($to)
            ->subject("Validation de l'adresse mail")
            ->text("Bonjour $username, Veuillez valider votre adresse mail en faisant un copier-coller du lien ci-dessous.
            $url merci et bienvenue")
            ->html("
                <h2>Bonjour $username, </h2>
                <p>
                Veuillez valider votre adresse mail en cliquant sur le lien ou en faisant un copier-coller du lien ci-dessous.
                </p>
                <a href='$url'>
                    $url
                </a>
                <p> merci et bienvenue</p>
                ");

        $mailer->send($email);

        return new Response("", Response::HTTP_OK);
    }
}
