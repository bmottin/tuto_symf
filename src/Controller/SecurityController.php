<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{

    protected $em;

    // on instancie une instance de manager pour palier le soucis d'auto-wiring
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @Route("/registration", name="security_registration")
     */
    public function registration(Request $request, UserPasswordEncoderInterface $encoder)
    {
        // un nouvel utilisateur
        $user = new User();
        // qui va completer le form si le user contient des infos
        $form = $this->createForm(RegistrationType::class, $user);
        // le formulaire va gerer la requete HTTP (GET/POST)
        $form->handleRequest($request);
        // si requete de type POST et que les champs sont valides
        if ($form->isSubmitted() && $form->isValid()) {

            $user->setDisplayName($user->getUsername());

            $hash = $encoder->encodePassword($user, $user->getPassword());
    
            // $hash = $argon2i$v=19$m=65536,t=4,p=1$TDh1Ny51MEdpS01CaWI0cA$m2rRwseLtwCuHsPa7TK4Epa8zkrQk7tp1eRcBXSno88
            $user->setPassword($hash);

            // on donne un role utilisateur à notre user
            $user->setRoles(['ROLE_USER']);

            $user->setVerificationCode($this->generateUniqueId());
            $user->setIsValidate(false);

            $user->setCreatedAt(new \DateTime());
            $user->setUpdatedAt(new \DateTime());

            $this->em->persist($user);
            $this->em->flush();

            // redirige le flux vers un autre controller
            $this->forward('\App\Controller\MailController::mailRegister', [
                'to'=>$user->getEmail(),
                'username'=>$user->getUsername(),
                'validationCode'=>$user->getVerificationCode()
            ]);

            $this->addFlash('notice', 'veuillez valider votre adresse mail pour finalisé l\'inscription');
            return $this->redirectToRoute("home");
        }

        return $this->render('security/registration.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/login", name="security_login")
     */
    public function login(AuthenticationUtils $au)
    {
        $error = $au->getLastAuthenticationError();
        $username = $au->getLastUsername();
        return $this->render('security/login.html.twig', [
            'lastUserName' => $username, 
            'error' => $error
        ]);
    }

    /**
     * @Route("/logout", name="security_logout")
     */
    public function logout()
    {
        
    }


    /**
     * @Route("/validationUser/{email}/{validationCode}", name="validationEmail")
     */
    public function validationEmail($email, $validationCode){

        // on recupere le manager (se qui permet de persister nos info)
        $entityManager = $this->getDoctrine()->getManager();
        // on recuperer le repo(qui permet d'acces au données)
        $entityRepo = $this->getDoctrine()->getRepository('\App\Entity\User');
        // on verifie qu'il yu a une coresspondance entre notre mail et le code de verif
        $user = $entityRepo->findOneBy(['email'=>$email, 'verificationCode' => $validationCode]);
        
        // s'il y a une correspondance
        if($user) {
            // on valide l'utilisateur
            $user->setIsValidate(true);
            // et j'enregistre
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash('notice', "Félicitation votre adresse mail à été validé");
        }

        return $this->redirectToRoute("home");
    }

    public function generateUniqueId() {
        return md5(uniqid());
    }
}
