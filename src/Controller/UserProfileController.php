<?php

namespace App\Controller;

use App\Form\UserEditProfileType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserProfileController extends AbstractController 
{
    /**
     * @Route("/user/profile/{username}", name="user_profile")
     */
    public function userProfile(UserRepository $userRepository, $username)
    {
        
        $currentLoggedUser = $this->getUser();
        // show the update button in template
        $canUpdate = false;
        // if the user is logged and his username is the same as the profile username
        if(!is_null($currentLoggedUser) && $currentLoggedUser->getUsername() == $username){
            $canUpdate = true;
        }
        
        return $this->render('user_profile/profile.html.twig', [
            'user' => $userRepository->findOneBy(['username' => $username]),
            'canUpdate' => $canUpdate
        ]);
    }

    /**
     * @Route("/user/profile/edit/{username}", name="user_edit_profile")
     */
    public function userEditProfile(UserRepository $userRepository, $username, Request $request, EntityManagerInterface $entityManager, UserPasswordEncoderInterface $encoder)
    {
        
        $currentLoggedUser = $this->getUser();
        // show the update button in template
        
        // if the user is logged and his username is the same as the profile username
        if(!is_null($currentLoggedUser) && $currentLoggedUser->getUsername() == $username){

            $user = $this->getUser();
            $form = $this->createForm(UserEditProfileType::class, $user);

            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){
                $user->setUpdatedAt(new \DateTime());
                $hash = $encoder->encodePassword($user, $user->getPassword());
                $user->setPassword($hash);

                $entityManager->persist($user);
                $entityManager->flush();
            }

            return $this->render('user_profile/profile_edit.html.twig', [
                'form' => $form->createView(),
                'user' => $user
            ]);    

        }  else {
            // si ce n'est pas le bon user, on le redirige vers la home
            // + message flash 
            $this->addFlash('error', "Action non autorisé");
            // TODO create error page 
            return $this->redirectToRoute('home', [], 302);
        }
        
       
    }


}
